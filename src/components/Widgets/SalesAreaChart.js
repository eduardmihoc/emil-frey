/**
 * Users Area Chart Widget
 */
import React from 'react';
import CountUp from 'react-countup';

// chart
import TinyAreaChart from 'Components/Charts/TinyAreaChart';

// chart config
import ChartConfig from 'Constants/chart-config';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box
import { RctCard, RctCardContent } from 'Components/RctCard';

// helpers
import { hexToRgbA } from 'Helpers/helpers';

const UsersAreaChart = ({ data }) => (
    <RctCard customClasses="vertical-red-orange">
        <RctCardContent>
            <div className="clearfix">
                <div className="float-left">
                    <h3 className="mb-15 fw-semi-bold"><IntlMessages id="widgets.sales" /></h3>
                    <div className="d-flex">
                        <div className="mr-50">
                            <span className="fs-14 d-block"><IntlMessages id="widgets.thisWeek" /></span>
                            <CountUp separator="," className="counter-point white" start={0} end={data.today} duration={5} useEasing={true} />
                        </div>
                    </div>
                </div>
                <div className="float-right hidden-md-down">
                    <div className="featured-section-icon__white">
                        <i className="zmdi zmdi-calendar-note" />
                    </div>
                </div>
            </div>
        </RctCardContent>
        <TinyAreaChart
            label="Sales"
            chartdata={data.chartData.data}
            labels={data.chartData.labels}
            backgroundColor={hexToRgbA(ChartConfig.color.info, 0.1)}
            borderColor={hexToRgbA(ChartConfig.color.white, 3)}
            lineTension="0.5"
            height={70}
            gradient
            hideDots={false}
        />
    </RctCard>
);

export default UsersAreaChart;
