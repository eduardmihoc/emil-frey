/**
 * Orders Area Chart Widget
 */
import React from 'react';
import CountUp from 'react-countup';

// chart
import TinyAreaChart from 'Components/Charts/TinyAreaChart';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// chart config
import ChartConfig from 'Constants/chart-config';

// rct card box
import { RctCard, RctCardContent } from 'Components/RctCard';

// helpers
import { hexToRgbA } from 'Helpers/helpers';

const VehicleSalesChart = ({ data }) => (
    <RctCard customClasses="vertical-green">
        <RctCardContent>
            <div className="clearfix">
                <div className="float-left">
                    <h3 className="mb-15 fw-semi-bold"><IntlMessages id="widgets.vehiclesSold" /></h3>
                    <div className="d-flex">
                        <div className="">
                            <CountUp separator="," className="counter-point white" start={0} end={data.totalRevenue} duration={5} useEasing={true} />
                        </div>
                    </div>
                </div>
                <div className="float-right hidden-md-down">
                    <div className="featured-section-icon__white">
                        <i className="zmdi zmdi-chart" />
                    </div>
                </div>
            </div>
        </RctCardContent>
        <TinyAreaChart
            label="Orders"
            chartdata={data.chartData.data}
            labels={data.chartData.labels}
            backgroundColor={hexToRgbA(ChartConfig.color.white, 0.1)}
            borderColor={hexToRgbA(ChartConfig.color.white, 3)}
            lineTension="0"
            height={70}
            gradient
            hideDots={false}
        />
    </RctCard>
);

export default VehicleSalesChart;
