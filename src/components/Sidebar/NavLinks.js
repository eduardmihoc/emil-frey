// sidebar nav links
export default {
	mainNavigation: [
        {
            "menu_title": "sidebar.dashboard",
            "menu_icon": "zmdi zmdi-view-dashboard",
            "path": "/app/dashboard",
            "child_routes": null
        },
        {
            "menu_title": "sidebar.vehicle",
            "menu_icon": "zmdi zmdi-settings",
            "path": "/app/vehicle-model",
            "child_routes": null
        },
        {
            "menu_title": "sidebar.vin",
            "menu_icon": "zmdi zmdi-chart",
            "path": "/app/vin",
            "child_routes": null
        },
        {
            "menu_title": "sidebar.vehicleRegistration",
            "menu_icon": "zmdi zmdi-border-all",
            "path": "/app/vehicle-registration",
            "child_routes": null
        },
        {
            "menu_title": "sidebar.hsn",
            "menu_icon": "zmdi zmdi-check-square",
            "path": "/app/hsn",
            "child_routes": null
        },
        {
            "menu_title": "sidebar.countries",
            "menu_icon": "zmdi zmdi-pin",
            "path": "/app/countries",
            "child_routes": null
        },
        {
            "menu_title": "sidebar.userProfile",
            "menu_icon": "zmdi zmdi-account-circle",
            "path": "/app/userProfile",
            "child_routes": null
        }
	]
}
