import React, { Component } from 'react';
import {
	ResponsiveContainer,
	LineChart,
	Line,
	XAxis,
	YAxis,
	CartesianGrid,
	Tooltip,
	Legend
} from 'recharts';
import ChartConfig, { tooltipStyle, tooltipTextStyle } from 'Constants/chart-config';

const data = [
	{ name: 'January', Vehicles: 100, Services: 240 },
	{ name: 'March', Vehicles: 300, Services: 139 },
	{ name: 'May', Vehicles: 150, Services: 400 },
	{ name: 'July', Vehicles: 278, Services: 390 },
];

class LineChartComponent extends Component {
	render() {
		return (
			<ResponsiveContainer width='100%' height={300}>
				<LineChart data={data}>
					<XAxis dataKey="name" stroke={ChartConfig.axesColor} />
					<YAxis stroke={ChartConfig.axesColor} />
					<CartesianGrid vertical={false} stroke={ChartConfig.chartGridColor} />
					<Tooltip wrapperStyle={tooltipStyle} cursor={false} itemStyle={tooltipTextStyle} labelStyle={{ display: 'none' }} />
					<Legend />
					<Line type="monotone" dataKey="Services" stroke={ChartConfig.color.primary} activeDot={{ r: 8 }} />
					<Line type="monotone" dataKey="Vehicles" stroke={ChartConfig.color.info} activeDot={{ r: 8 }} />
				</LineChart>
			</ResponsiveContainer>
		);
	}
}

export default LineChartComponent;
