/**
 * Ecommerce Dashboard
 */

import React, {Component} from 'react'
import {Helmet} from "react-helmet";
// intl messages
import IntlMessages from 'Util/IntlMessages';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// rct collapsible card
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import {
    VehicleWeekSalesWidget,
    VehicleSalesChartWidget,
    VehicleStockChartWidget,
    SupportRequest,
} from "Components/Widgets";

// widgets data
import {
    visitorsData,
    salesData,
    ordersData,
} from './data';
import LineChart from "Routes/charts/recharts/line-chart";

export default class EcommerceDashboard extends Component {
    render() {
        const {match} = this.props;
        return (
            <div className="ecom-dashboard-wrapper">
                <Helmet>
                    <title>Dashboard</title>
                    <meta name="description" content="Reactify Ecommerce Dashboard"/>
                </Helmet>
                <PageTitleBar title={<IntlMessages id="sidebar.dashboard"/>} match={match}/>
                <div className="row">
                    <div className="col-sm-6 col-md-4 w-xs-half-block">
                        <VehicleStockChartWidget
                            data={visitorsData}
                        />
                    </div>

                    <div className="col-sm-12 col-md-4 w-xs-half-block">
                        <VehicleSalesChartWidget
                            data={ordersData}
                        />
                    </div>
                    <div className="col-sm-6 col-md-4 w-xs-full">
                        <VehicleWeekSalesWidget
                            data={salesData}
                        />
                    </div>
                </div>
                <div className="row">
                    <RctCollapsibleCard
                        heading={<IntlMessages id="widgets.salesByType"/>}
                        colClasses="col-sm-12 col-md-6 col-xl-6  w-xs-full"
                        collapsible
                        reloadable
                        closeable
                        fullBlock
                        customClasses="overflow-hidden"
                    >
                        <LineChart/>
                    </RctCollapsibleCard>
                    <RctCollapsibleCard
                        colClasses="col-sm-12 col-md-6 col-xl-6 w-xs-full"
                        heading={<IntlMessages id="widgets.supportRequest"/>}
                        collapsible
                        reloadable
                        closeable
                        fullBlock
                        customClasses="overflow-hidden"
                    >
                        <SupportRequest/>
                    </RctCollapsibleCard>
                </div>
            </div>
        )
    }
}
